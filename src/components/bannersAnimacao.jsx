import React, { Component } from "react";
import "./barraSuperior";
import BarraSuperior from "./barraSuperior";
import BreadCrumb from "./breadCrumb";
import MenuLateral from "./menuLateral";
import Footer from "./footer";
import MenuUsuario from "./menuUsuario";

class BannerAnimacao extends Component {
  state = {};
  render() {
    return (
      <div className="loggedin">
        <MenuLateral />
        <main className="main">
          <BarraSuperior />
          <section className="section section__banner">
            <BreadCrumb />
            <div className="step" />
          </section>
          <Footer />
        </main>
        <MenuUsuario />
      </div>
    );
  }
}

export default BannerAnimacao;
