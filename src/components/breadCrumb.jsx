import React, { Component } from "react";

class BreadCrumb extends Component {
  state = {};
  render() {
    return (
      <div className="banner-steps-wrapper">
        {/* inicio bread crumb component */}
        <div className="banner-steps">
          <ol className="banner-steps__ol">
            <li className="banner-steps__li banner-steps__li--active">
              <a href="#" className="banner-steps__a">
                <div className="banner-steps__icon">
                  <i className="fas fa-cloud-upload-alt" />
                </div>
                <div className="banner-steps__upload-li">Upload do arquivo</div>
              </a>
            </li>
            <li className="banner-steps__li">
              <a href="#" className="banner-steps__a">
                <div className="banner-steps__icon">
                  <i className="fas fa-film" />
                </div>
                Animações
              </a>
            </li>
            <li className="banner-steps__li">
              <a href="#" className="banner-steps__a">
                <div className="banner-steps__icon">
                  <i className="fas fa-globe" />
                </div>
                ClickTags
              </a>
            </li>
            <li className="banner-steps__li">
              <a href="#" className="banner-steps__a">
                <div className="banner-steps__icon">
                  <i className="fas fa-cloud-download-alt" />
                </div>
                Baixar o pacote
              </a>
            </li>
          </ol>
        </div>
        {/* fim inicio bread crumb component */}
      </div>
    );
  }
}

export default BreadCrumb;
