import React, { Component } from "react";

class MenuLateral extends Component {
  state = {};
  render() {
    return (
      // {/* inicio da barra lateral */}
      <aside className="menu-wrapper menu-wrapper--collapsed">
        <h1 className="menu-wrapper__logo">StormAds</h1>
        <nav className="menu">
          <div className="menu__collapse-menu">
            <i className="fas fa-angle-right" />
          </div>
          <ul className="menu-list">
            <li className="menu-list__item">
              <a href="#" className="menu-list__link">
                <i className="fas fa-home" /> <span>Início</span>
              </a>
            </li>
            <li className="menu-list__item">
              <a href="#" className="menu-list__link">
                <i className="fas fa-building" /> <span>Clientes</span>
              </a>
            </li>
            <li className="menu-list__item">
              <a href="#" className="menu-list__link">
                <i className="fas fa-id-card-alt" /> <span>Usuários</span>
              </a>
            </li>
            <li className="menu-list__item">
              <a href="#" className="menu-list__link menu-list__link--active">
                <i className="fas fa-paint-brush" /> <span>Campanhas</span>
              </a>
            </li>
          </ul>
        </nav>
      </aside>
      // {/* fim da barra lateral  */}
    );
  }
}

export default MenuLateral;
{
}
