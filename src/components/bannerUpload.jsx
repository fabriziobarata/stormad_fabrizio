import React, { Component } from "react";

class BannerUpload extends Component {
  state = {};
  render() {
    return (
      <div className="psdfile">
        <div className="psdfile__wrapper">
          <div className="psdfile__dialog">
            <h4>Faça o upload do arquivo PSD</h4>
            <p>
              O seu arquivo deve seguir a estrutura de pastas e camadas como no
              exemplo abaixo:
            </p>
            <ul>
              <li>
                <i className="fas fa-folder-open"></i> STEP 1
                <ul>
                  <li>
                    <i className="far fa-image"></i> IMG_FUNDO
                  </li>
                </ul>
              </li>
              <li>
                <i className="fas fa-folder-open"></i> STEP 2
                <ul>
                  <li>
                    <i className="far fa-image"></i> IMG_FUNDO_2
                  </li>
                  <li>
                    <i className="fas fa-font"></i> TXT_2
                  </li>
                </ul>
              </li>
              <li>
                <i className="fas fa-folder-open"></i> STEP 3
                <ul>
                  <li>
                    <i className="far fa-image"></i> IMG_FUNDO_3
                  </li>
                  <li>
                    <i className="fas fa-font"></i> TXT_3
                  </li>
                </ul>
              </li>
            </ul>

            <label className="botao" for="project__input-upload-file">
              <i className="fas fa-file-upload"></i> Escolha o arquivo
            </label>

            <input
              type="file"
              id="project__input-upload-file"
              className="project__input-upload-file"
              accept=".psd"
            />
          </div>
        </div>

        <div className="psdfile__footer">
          <button className="botao on" Onclick="uploadPsdFile">
            Continuar
          </button>
        </div>
      </div>
    );
  }
}

export default BannerUpload;
