import React, { Component } from "react";

class BarraSuperior extends Component {
  state = {};
  render() {
    return (
      <header className="header">
        <div className="header__tit-wrapper">
          <span className="header__tit header__tit--previous">Campanha #1</span>
          <i className="fas fa-angle-right" />
          <span className="header__tit">Novo projeto #1</span>
        </div>
        <div className="header__search-wrapper">
          <div className="header__search">
            <i className="fas fa-search" />
          </div>
        </div>
        <div className="header__profile-wrapper">
          <div className="header__profile">
            <i className="fas fa-user" />
          </div>
          <div className="header__profile-menu">
            <i className="fas fa-caret-down profile-menu-icon" />
          </div>
        </div>
      </header>
    );
  }
}

export default BarraSuperior;
