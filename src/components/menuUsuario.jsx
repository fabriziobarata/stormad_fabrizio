import React, { Component } from "react";

class MenuUsuario extends Component {
  state = {};
  render() {
    return (
      <nav className="profile-menu profile-menu--fadeOut">
        <ul className="profile-menu__lista">
          <li className="profile-menu__item">
            <a href="#" className="profile-menu__link">
              <i className="fas fa-id-card" /> Meus dados
            </a>
          </li>
          <li className="profile-menu__item">
            <a href="#" className="profile-menu__link">
              <i className="fas fa-users" /> Usuários
            </a>
          </li>
          <li className="profile-menu__item">
            <a href="#" className="profile-menu__link">
              <i className="fas fa-power-off" /> Sair
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default MenuUsuario;
