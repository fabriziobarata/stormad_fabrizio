import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "./components/bannersAnimacao";
import BannerAnimacao from "./components/bannersAnimacao";
import "./css/estilo.css";

function App() {
  return (
    <div className="App">
      <BannerAnimacao />
    </div>
  );
}

export default App;
